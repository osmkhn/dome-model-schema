# README #

This project builds an XSD schema for "DOME Model" .dm files that are created by DOME Client and then hosted or executed on DOME Server. Once this is done, it will open up the ability for developers to consume models much more easily and reliably.

DM files look like this:

    <model type="DomeModel" id="314b1174-b698-1004-8f8b-8912c506e25c" name="math model">
        <modelinfo>
            <version>0.0.1</version>
        </modelinfo>
        <parameters>
          ...
        </parameters>
        <relations>
         ...    
        </relation>
           
        <visualizations/>
        <contexts>
           ...
        </contexts>
        <mappings>
            ...
        </mappings>
    </model>



### Platform ###
XML

### How do I get set up? ###

TODO

### Contribution guidelines ###

TODO

### Disclaimer ###
This repo is a volunteer effort to build clean, concise and approachable file schema documentation for the DOME project. Copyright for initial schemas and APIs rests with DMDII and the Project DMC team.